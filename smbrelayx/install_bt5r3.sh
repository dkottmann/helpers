#!/bin/bash

INSTALL_LOC=/root/Desktop
VENV=smbvenv

cd /tmp
echo "[+] Installing pip and virtualenv"
apt-get install python-pip python-virtualenv
echo "[+] Downloading impacket python tarball"
wget https://impacket.googlecode.com/files/impacket-0.9.10.tar.gz -O impacket-0.9.10.tar.gz

cd $INSTALL_LOC
echo "[+] Creating environment"
virtualenv $VENV
source $VENV/bin/activate
echo "[+] Installing impacket tarball to virtual environment"
pip install /tmp/impacket-0.9.10.tar.gz

echo "[+] Patching smbrelayx.py"
cat > /tmp/smbrelayx.patch <<EOF
@@ -345,7 +345,7 @@
         smbConfig.set('IPC$','comment','')
         smbConfig.set('IPC$','read only','yes')
         smbConfig.set('IPC$','share type','3')
-        smbConfig.set('IPC$','path')
+        smbConfig.set('IPC$','path', None)
 
         self.server = smbserver.SMBSERVER(('0.0.0.0',445), config_parser = smbConfig)
         self.server.processConfigFile()
EOF
patch $VENV/bin/smbrelayx.py < /tmp/smbrelayx.patch

echo "[+] Cleaning up"
rm /tmp/smbrelayx.patch /tmp/impacket-0.9.10.tar.gz
deactivate

cat <<EOF
========================================================
                 Done!

  To run smbrelayx.py you must perform the following:

	source $INSTALL_LOC/$VENV/bin/activate
	smbrelayx.py [options]

  Happy hacking!
========================================================
EOF

